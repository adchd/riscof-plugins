### Riscof Plugin for riscvOVPsim

- Installation
The steps for installation and usage can be found  here: [riscv-ovpsim/README.md](riscv-ovpsim/README.md) and here: [riscv-ovpsim/doc/riscvOVPsim_User_Guide.pdf](riscv-ovpsim/doc/riscvOVPsim_User_Guide.pdf).

- Config file entry
```
DUTPlugin=OVPsim
#ReferencePlugin=OVPsim
```

- Export command
```
pwd=pwd;export PYTHONPATH=$pwd:$PYTHONPATH;
```